function [ wynik ] = F90WriteX( nazwa, perme_array, elcond_array, x_step, cf, sf )
    %Write Permeability and ELectric Conductivity Array generated in matlab,
    %linear displacement 'x_step',sinus and cosinus of array rotation
    % into fortran *.f90 file (!perme_cond_p2.f90!)

%Array transposition for Fortran column-major initialization
perme_arrayT = perme_array';
elcond_arrayT = elcond_array';

%Permeability array header:
a{1}='    DATA perme_array &';
a{2}='    / &';

%Permeability Array Data Initialization
a{3}=[num2str(perme_arrayT(1,:),'%1.0fd0, '),' &'];
a{4}=[num2str(perme_arrayT(2,:),'%1.0fd0, '),' &'];
a{5}=[num2str(perme_arrayT(3,:),'%1.0fd0, '),' &'];
a{6}=[num2str(perme_arrayT(4,:),'%1.0fd0, '),' &'];
a{7}=[num2str(perme_arrayT(5,:),'%1.0fd0, '),' &'];
a{8}=[num2str(perme_arrayT(6,:),'%1.0fd0, '),' &'];
a{9}=[num2str(perme_arrayT(7,:),'%1.0fd0, '),' &'];
a{10}=[num2str(perme_arrayT(8,:),'%1.0fd0, '),' &'];
a{11}=[num2str(perme_arrayT(9,:),'%1.0fd0, '),' &'];
a{12}=[num2str(perme_arrayT(10,:),'%1.0fd0, '),' &'];
a{13}=[num2str(perme_arrayT(11,:),'%1.0fd0, '),' &'];
a{14}=[num2str(perme_arrayT(12,:),'%1.0fd0, '),' &'];
a{15}=[num2str(perme_arrayT(13,:),'%1.0fd0, '),' &'];
a{16}=[num2str(perme_arrayT(14,:),'%1.0fd0, '),' &'];
a{17}=[num2str(perme_arrayT(15,:),'%1.0fd0, '),' &'];
a{18}=[num2str(perme_arrayT(16,1:15),'%1.0fd0, '),' ',num2str(perme_arrayT(16,16),'%1.0fd0 '),' &'];
a{19}='    /';
%
%%Electric Conductivity Array Header
a{20}='     DATA elcond_array &';
a{21}='    / &';
%
%%Electric Conductivity Data Initialization
a{22}=[num2str(elcond_arrayT(1,1:7),'%1.2fd7, '),' &'];
a{23}=[num2str(elcond_arrayT(1,8:16),'%1.2fd7, '),' &'];
a{24}=[num2str(elcond_arrayT(2,1:7),'%1.2fd7, '),' &'];
a{25}=[num2str(elcond_arrayT(2,8:16),'%1.2fd7, '),' &'];
a{26}=[num2str(elcond_arrayT(3,1:7),'%1.2fd7, '),' &'];
a{27}=[num2str(elcond_arrayT(3,6:16),'%1.2fd7, '),' &'];
a{28}=[num2str(elcond_arrayT(4,1:7),'%1.2fd7, '),' &'];
a{29}=[num2str(elcond_arrayT(4,8:16),'%1.2fd7, '),' &'];
a{30}=[num2str(elcond_arrayT(5,1:7),'%1.2fd7, '),' &'];
a{31}=[num2str(elcond_arrayT(5,8:16),'%1.2fd7, '),' &'];
a{32}=[num2str(elcond_arrayT(6,1:7),'%1.2fd7, '),' &'];
a{33}=[num2str(elcond_arrayT(6,8:16),'%1.2fd7, '),' &'];
a{34}=[num2str(elcond_arrayT(7,1:7),'%1.2fd7, '),' &'];
a{35}=[num2str(elcond_arrayT(7,8:16),'%1.2fd7, '),' &'];
a{36}=[num2str(elcond_arrayT(8,1:7),'%1.2fd7, '),' &'];
a{37}=[num2str(elcond_arrayT(8,8:16),'%1.2fd7, '),' &'];
a{38}=[num2str(elcond_arrayT(9,1:7),'%1.2fd7, '),' &'];
a{39}=[num2str(elcond_arrayT(9,8:16),'%1.2fd7, '),' &'];
a{40}=[num2str(elcond_arrayT(10,1:7),'%1.2fd7, '),' &'];
a{41}=[num2str(elcond_arrayT(10,8:16),'%1.2fd7, '),' &'];
a{42}=[num2str(elcond_arrayT(11,1:7),'%1.2fd7, '),' &'];
a{43}=[num2str(elcond_arrayT(11,6:16),'%1.2fd7, '),' &'];
a{44}=[num2str(elcond_arrayT(12,1:7),'%1.2fd7, '),' &'];
a{45}=[num2str(elcond_arrayT(12,8:16),'%1.2fd7, '),' &'];
a{46}=[num2str(elcond_arrayT(13,1:7),'%1.2fd7, '),' &'];
a{47}=[num2str(elcond_arrayT(13,8:16),'%1.2fd7, '),' &'];
a{48}=[num2str(elcond_arrayT(14,1:7),'%1.2fd7, '),' &'];
a{49}=[num2str(elcond_arrayT(14,8:16),'%1.2fd7, '),' &'];
a{50}=[num2str(elcond_arrayT(15,1:7),'%1.2fd7, '),' &'];
a{51}=[num2str(elcond_arrayT(15,8:16),'%1.2fd7, '),' &'];
a{52}=[num2str(elcond_arrayT(16,1:7),'%1.2fd7, '),' &'];
a{53}=[num2str(elcond_arrayT(16,8:15),'%1.2fd7, '),' ',num2str(elcond_arrayT(16,16),'%1.2fd7 '),' &'];
a{54}='    /';
%
%%End of Arrays Initialization
a{55}='';
a{56}='!End of data initialization';
a{57}='';
%
%%Declaration of x_step, sinus and cosinus
a{58}='    REAL(KIND=dp) :: &';
a{59}=['    x_step = ',num2str(x_step,'%1.4f'), 'd0 ,& !x_step - odpowiednik x1'];
a{60}=['    cf = ',num2str(cf,'%1.4f'), 'd0 ,&'];
a{61}=['    sf = ',num2str(sf,'%1.4f'), 'd0 ,&'];

y_db = x_step + (-0.016);
y_ub = x_step + (0.016);

a{62}=['    y_db = ',num2str(y_db,'%1.4f'), 'd0 ,&'];
a{63}=['    y_ub = ',num2str(y_ub,'%1.4f'), 'd0 ,&'];
a{64}='';

%Write to file
FID = fopen(nazwa, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

wynik=1;

end

