MODULE interpUtils

    Use DefUtils
    Implicit None
    INTEGER, PARAMETER :: arr_up_bnd = 16, arr_low_bnd = 1

    REAL(kind=dp), DIMENSION(arr_low_bnd:arr_up_bnd,arr_low_bnd:arr_up_bnd) :: perme_array, elcond_array
!Here goes data initialization (generated with F90WriteX):
