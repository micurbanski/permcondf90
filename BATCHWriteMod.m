function [ wynik ] = BATCHWrite( filename )
%BATCHWRITE prepare batch files in catalog for core n

a{1}='#!/bin/sh -f';
a{2}='netgen -geofile=ECTmesh.geo -meshfile=ECTmesh.msh -meshfiletype="Gmsh2 Format" -batchmode';
a{3}='ElmerGrid 14 2 ECT.msh -autoclean';
a{4}='rm ECTmesh.msh';
a{5}='cat perme_cond_p1.f90 perme_cond_p2.f90 perme_cond_p3.f90 > perme_cond.f90';
a{6}='elmerf90 perme_cond.f90 -o perme';
a{7}='elmerf90 integ.f90 -o integ';

FID = fopen(filename, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

[output, text]=system(["chmod 755 " nazwa]);    

wynik=1;

end

